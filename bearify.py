#!/bin/python3

# Bearify:
# Run on a Code License to create a completely new, Bear-License variant of it.

# NOTICE: This program is Tri-Licensed! Please see the repository for a copy of all 3 licenses.
# Only the main license has been included here.
#
# MIT License
#
# Copyright (c) 2022 Baob Koiss
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Needed to check if file exists
import os
# Used to just replace words, not touching punctuation.
import re
from io import BufferedReader
# Used to substitute seeds for emails
from random import randint

# hash() is randomized, by using a sha1 hash from hashlib instead we get reproducable bear licenses, based on content alone.
import hashlib


# List of bear words that are randomly picked from
BEAR_SOUNDS = [
	"REAAGH",
	"RAAGH",
	"ROARR",
	"RAGGHG",
	"ROAAARRR",
	"RREAOOGH",
	"RAAARH",
	"GHROOOAHG",
	"RAAARORORHH"
]

# These words are hard-replaced
# I sourced them from getting together a few popular licenses and getting the word frequencies.
# It's not in the order of frequency, just whatever I could think of.
HARD_BEARS = {
	"copyright": "AAAUUUGGGHH",
	"copyleft": "HHGGGUUUAAA",
	"software": "Salmon",

	"the": "rawr",
	"license": "BEAR LICENSE",
	"distribute": "hunt",
	"library": "BEARS",
	"copy": "BEAR CLONING",
	"conditions": "BEAR REQUIREMENTS",
	"not": "PANDA",
	"of": "BEARWAY",
	"this": "BEARTH",
	"source": "OF THE BEAR",
	"code": "Salmon HUNTing TECHNIQUES",
	"gnu": "FEET",
	"public": "BEAR REPUBLIC",
}

# A list of names to use for the bear emails.
# Also used to generate the fictitious domains for the emails.
BEAR_NAMES = [
	"pooh",
	"baloo",
	"yogi",
	"paddington",
	"bubu",
	"po",
	"lotso",
	"byrnison",
	"fozzie",
	"boog",
	"bungle",
	"ted",
	"rupert",
	"bobo",
	"gohin",
	"basil",
]

# List of domain TLDs to use for fake bear emails.
BEAR_TLD = [
	"bear",
	"biearz",
	"gay", # haha
	"bearmail",
	"mailb",
]

def get_bear_word(word: str) -> str:
	"""Returns the correct bear word for the given word."""
	if word.lower() in HARD_BEARS:
		return HARD_BEARS[word.lower()]

	seed = hashlib.sha1(word.encode('utf-8')).hexdigest()
	seed = int(seed, 16)
	if len(word) > 1:
		return BEAR_SOUNDS[seed % len(BEAR_SOUNDS)]
	else:
		return "BEAR"[seed % len("BEAR")]

def get_bear_number(number: str) -> str:
	"""Returns the correct bear number for the given number."""
	# Bear numbers are base 2 with the following symbols: E, A
	# And the numbers start with 'B' and end with 'R'.
	num = int(number)
	bear_number = "B{0:b}R".format(num).replace('0', 'A').replace('1', 'E')
	return bear_number

def generate_bear_domain(seed: int = -1) -> str:
	"""Generates a bear domain. Will never be valid."""
	bear_tld = BEAR_TLD[seed % len(BEAR_TLD)]
	bear_domains = [get_bear_word(str(seed))]
	while seed % 2 == 0 and seed > 0:
		seed >>= 1
		bear_domains.append(get_bear_word(str(seed)))

	return f"{','.join(bear_domains)},{bear_tld}".lower()

def generate_bear_url(seed: int = -1) -> str:
	return f"bearnet://{generate_bear_domain(seed)}"

def generate_bear_email(seed: int = -1) -> str:
	"""Returns & generates a brand new bear email. These 'sublty' ignore email rules to never be valid."""
	if seed < 0:
		seed = randint(0, 99999)

	bear_name = BEAR_NAMES[seed % len(BEAR_NAMES)]
	return f"{bear_name}@{generate_bear_domain(seed)}"

def bearify_license_buf(file: BufferedReader) -> str:
	"""Bearifies an entire license by file buffer. Returns the entire bear-ified license."""

	contents = file.read()
	# Regex is probably overkill. Cry about it.

	# Bear the words.
	outbuffer = re.sub(r"\b([^\W\d]+)\b", lambda a : get_bear_word(a.group(0)), contents)

	# Bear the numbers.
	outbuffer = re.sub(r"(\d+)", lambda a : get_bear_number(a.group(0)), outbuffer)

	# Bear the emails.
	# very simple hand-crafted email detector
	outbuffer = re.sub(r"(\w+@\w+.\w+)", lambda a : generate_bear_email(len(a.group(0))), outbuffer )

	# Bear the domains!
	# simple hand-crafted URL detector.
	# With paths: (\w*:\/\/(?:\w+\.)*\w+)((?:\/.*\/)?)
	# Without Paths: (\w*:\/\/(?:\w+\.)*\w+)
	outbuffer = re.sub(r"(\w*:\/\/(?:\w+\.)*\w+)", lambda a: generate_bear_url(len(a.group(0))), outbuffer)

	return outbuffer


def bearify_license_path(path: str) -> str:
	"""Bearifies an entire license by path. Returns the entire bear-ified license."""

	if os.path.exists(path) and os.path.isfile(path):
		with open(path, 'r') as f:
			return bearify_license_buf(f)
	else:
		raise FileNotFoundError("Couldn't find a suitable license to bear.")

if __name__ == "__main__":
	# We were ran as main

	from sys import argv

	if len(argv) > 1:
		try:
			print(bearify_license_path(argv[1]))
		except FileNotFoundError as e:
			print(e)