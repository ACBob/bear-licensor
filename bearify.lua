#!/usr/bin/lua

--[[
	NOTICE: This program is Tri-Licensed! Please see the repository for a copy of all 3 licenses.
	Only the main license has been included here.

	MIT License

	Copyright (c) 2022 Baob Koiss

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

]]

-- Lua implementation of the Bearify CLI.
-- Note that this won't produce licenses that are the same as the python version.
-- Arguably, the python impl. will give more "random" results, as it uses the SHA1 hashing algorithm because it was just sitting around.
-- However, this Lua version uses a CRC16 checksum of words instead, which was easier to implement by hand, lol.

-- Bearify:
-- Run on a Code License to create a completely new, Bear-License variant of it.

-- List of bear words that are randomly picked from
local BEAR_SOUNDS = {
	"REAAGH",
	"RAAGH",
	"ROARR",
	"RAGGHG",
	"ROAAARRR",
	"RREAOOGH",
	"RAAARH",
	"GHROOOAHG",
	"RAAARORORHH"
}

-- These words are hard-replaced
-- The actual ones to be replaced were from getting some popluar licenses and choosing funny ideas for each.
-- These aren't in list of the most common, but interesting to note, "The" is the most common, followed by "of".
local HARD_BEARS = {
	copyright = "AAAUUUGGGHH",
	copyleft = "HHGGGUUUAAA",
	software = "Salmon",

	the = "rawr",
	license = "BEAR LICENSE",
	distribute = "hunt",
	library = "BEARS",
	copy = "BEAR CLONING",
	conditions = "BEAR REQUIREMENTS",
	["not"] = "PANDA",
	of = "BEARWAY",
	this = "BEARTH",
	source = "OF THE BEAR",
	code = "Salmon HUNTing TECHNIQUES",
	gnu = "FEET",
	public = "BEAR REPUBLIC",
}

local BEAR_NAMES = {
	"pooh",
	"baloo",
	"yogi",
	"paddington",
	"bubu",
	"po",
	"lotso",
	"byrnison",
	"fozzie",
	"boog",
	"bungle",
	"ted",
	"rupert",
	"bobo",
	"gohin",
	"basil",
}

local BEAR_TLD = {
	"bear",
	"biearz",
	"gay", -- haha
	"bearmail",
	"mailb",
}

-- Implementation of a CRC16 checksum.
-- This isn't garuanteed to work predictably, or very well at all.
-- It's good enough for the uses it is used for, though, and that is pseudorandom from words.
function crc16(to_check)
	local crc = 0xFFFF
	local len = #to_check
	local x

	local i = 1
	while len > 0 do
		x = ((crc >> 8)) ~ to_check:byte(i)
		x = x ~ ((x >> 4))
		crc = ((crc << 8)) ~ ((crc << 12)) ~ ((crc << 5)) ~ x

		len = len - 1
		i = len + 1
	end

	return crc
end

------------------------
-- Returns the bear variant of a word.
-- @param word The word to get a variant of.
-- @return the bear word.
------------------------
function get_bear_word(word)
	assert(type(word) == 'string')

	if HARD_BEARS[word:lower()] ~= nil then
		return HARD_BEARS[word:lower()]
	end

	local checksum = crc16(word)

	if #word == 1 then
		return string.sub("BEAR", 1 + checksum % 4, 1 + checksum % 4)
	end

	return BEAR_SOUNDS[1 + checksum % #BEAR_SOUNDS]
end


------------------------
-- Returns the bear variant of a number.
-- @param number The number to get a variant of.
-- @return the bear number, as string.
------------------------
function get_bear_number(number)
	if type(number) == 'string' then
		string = tonumber(number)
	else
		assert(type(number) == 'number')
	end

	local bear_number = ""
	while number ~= 0 do
		if number % 2 == 0 then
			bear_number = bear_number .. 'A'
		else
			bear_number = bear_number .. 'E'
		end
		number = number >> 1
	end

	return 'B' .. bear_number:reverse() .. 'R'
end


------------------------
-- Returns a random Bear Domain, based on a seed.
-- If the seed is even, it can even produce subdomains.
-- @param seed The seed to use.
-- @return the bear domain.
------------------------
function generate_bear_domain(seed)
	assert(seed == nil or type(seed) == 'number')
	seed = seed or math.random(-99999, 99999)

	local bear_tld = BEAR_TLD[1 + seed % #BEAR_TLD]
	local bear_domains = get_bear_word(tostring(seed))

	while seed % 2 == 0 and seed > 0 do
		seed = seed >> 1
		bear_domains = bear_domains .. "." .. get_bear_word(tostring(seed))
	end

	return bear_domains:lower() .. '.' .. bear_tld
end


------------------------
-- Returns a random Bear Email, based on a seed.
-- @param seed The seed to use.
-- @return the bear email.
------------------------
function generate_bear_email(seed)
	assert(seed == nil or type(seed) == 'number')
	seed = seed or math.random(-99999, 99999)

	local bear_name = BEAR_NAMES[1 + seed % #BEAR_NAMES]
	return bear_name .. "@" .. generate_bear_domain(seed)
end


------------------------
-- Performs a bearification of the given string.
-- @param license The license to bearify
-- @return A bearified license.
------------------------
function bearify_license(license)
	assert(type(license) == 'string')

	-- Bear the words.
	license = license:gsub("(%a+)", get_bear_word)

	-- Bear the numbers.
	license = license:gsub("(%d+)", get_bear_number)

	-- Bear the emails.
	license = license:gsub("(%w+@%w+.%w+)", function(str) return generate_bear_email(#str) end)

	-- Bear the URLs.
	license = license:gsub("(%w+://[%w+\\.]*%w+)", function(str)
		return "bearnet://" .. generate_bear_domain(#str)
	end)

	return license
end


if not pcall(debug.getlocal, 4, 1) then
	-- we were ran as main
	local filepath = arg[1]
	local file = io.open(filepath, 'r')

	if file == nil then
		print("Could not open file " .. filepath)
		os.exit(false)
	end

	local content = file:read('*all')
	file:close()

	print(bearify_license(content))
end